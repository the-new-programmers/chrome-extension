let t = [];
let container;
let interval;
let node;

window.onload = (event) => {
    console.log('page is fully loaded');

    setTimeout(() => {

        node = document.createElement("h3");
        node.style.textAlign = 'center';
        node.style.color = 'rgb(185, 185, 185)';
        node.style.fontWeight = '300';
        node.style.background = '#000';
        node.style.direction = 'ltr';
        node.style.marginTop = '-21px';
        node.style.fontSize = '24px';
        node.style.display = 'none';


        container = document.querySelector('.ytd-video-primary-info-renderer');
        container.insertBefore(node, container.firstChild);

        trackChange();

    }, 2000);


    document.querySelector('.ytp-subtitles-button').onclick = () => {
        setTimeout(() => {
            trackChange();
        }, 1500);
    }

    const   css     = '.caption-window { opacity: 0 !important; }',
            head    = document.head || document.getElementsByTagName('head')[0],
            style   = document.createElement('style');

    head.appendChild(style);

    style.type = 'text/css';
    if (style.styleSheet){
        // This is required for IE8 and below.
        style.styleSheet.cssText = css;
    } else {
        style.appendChild(document.createTextNode(css));
    }
};



function trackChange() {
    if (document.querySelector('.ytp-subtitles-button').getAttribute('aria-pressed') === 'true' ) {

        document.querySelector('.caption-window').addEventListener('DOMSubtreeModified', function() {
            tackText();
        });

        clearInterval(interval);
        interval = setInterval(function() {
            tackText();
        }, 300);
    }
}


function tackText() {
    if (document.querySelector('.ytp-subtitles-button').getAttribute('aria-pressed') === 'true' ) {
        node.style.display = 'block';
        t = [];

        document.querySelectorAll('.ytp-caption-segment').forEach(e => t.push(e.innerText));
        if (t.length) {
            let newTxt = t.shift() + (t.length > 0 ? '<br>' + t.join(' ') : '');

            if (node.innerHTML !== newTxt) {
                node.innerHTML = newTxt;
            }
        }
    }
    else {
        clearInterval(interval);
        node.style.display = 'none';
    }
}

